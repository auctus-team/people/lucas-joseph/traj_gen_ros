cmake_minimum_required(VERSION 3.12)
project(traj_gen_ros VERSION 0.0.1 LANGUAGES CXX)
set (CMAKE_CXX_STANDARD 17)

## Add catkin
find_package(catkin REQUIRED
              traj_gen
              dynamic_reconfigure
              geometry_msgs
              message_generation
              actionlib
              actionlib_msgs
              eigen_conversions
              roscpp
              realtime_tools
              cmake_modules)

include_directories( 
    include
    ${CATKIN_INCLUDE_DIRS}
)

add_action_files(
      DIRECTORY action
      FILES Trajectory.action
      )

add_service_files(
  FILES
  TrajectoryLimits.srv
)
      
generate_messages(
  DEPENDENCIES
  geometry_msgs actionlib_msgs
  )

catkin_package(
  LIBRARIES ${PROJECT_NAME}
  INCLUDE_DIRS include
  CATKIN_DEPENDS traj_gen
)

add_library(${PROJECT_NAME}
            src/${PROJECT_NAME}.cpp)

add_dependencies(${PROJECT_NAME} ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})

## Specify libraries to link a library or executable target against
 target_link_libraries(${PROJECT_NAME}
    ${catkin_LIBRARIES}
    yaml-cpp
 )

 target_include_directories(${PROJECT_NAME} SYSTEM PUBLIC
 include
 ${EIGEN3_INCLUDE_DIRS}
 ${catkin_INCLUDE_DIRS}
)

# set_target_properties(${PROJECT_NAME} PROPERTIES OUTPUT_NAME ${PROJECT_NAME} PREFIX "")


