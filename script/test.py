#!/usr/bin/env python3

import rospy
import ptp_utils
import time
from geometry_msgs.msg import Pose

if __name__ == "__main__":
    try:
        rospy.init_node("trajectory_client_py")

        robot_init_pose = ptp_utils.get_link_pose("panda_link8")
        robot_init_pose.position.x = 0.5
        robot_init_pose.position.y = -0.3
        robot_init_pose.position.z = 0.5
        ptp_utils.set_max_velocity(translation=0.1)
        ptp_utils.set_max_acceleration(translation=1.3)
        ptp_utils.go_to_pose(robot_init_pose)
        robot_pose = ptp_utils.get_link_pose("panda_link8")

        time.sleep(2.0)
        robot_pose.position.y += 0.4
        ptp_utils.set_max_velocity(translation=0.3)
        ptp_utils.set_max_acceleration(translation=0.4)
        ptp_utils.go_to_pose(robot_pose)

    except rospy.ROSInterruptException:
        print("program interrupted before completion")
