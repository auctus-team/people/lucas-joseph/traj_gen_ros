#include <traj_gen_ros/traj_gen_ros.hpp>


namespace traj_gen_ros{

bool TrajGenRos::initialize(Eigen::Matrix4d pose_init)
{
    ros::NodeHandle node_handle;
    ROS_DEBUG_STREAM("Initializing Trajectory");
    current_state.pose.setFrame(pose_init);
    desired_state = current_state;
    ROS_DEBUG_STREAM("setting max velocity");
    ROS_DEBUG_STREAM("Trajectory Initialized");
    pose_des_publisher.init(node_handle, "desired_cartesian_pose", 1);
    vel_des_publisher.init(node_handle, "desired_cartesian_velocity", 1);
    acc_des_publisher.init(node_handle, "desired_cartesian_acceleration", 1);
    TrajectoryLimitsService = node_handle.advertiseService("/panda/setTrajectoryLimits", &TrajGenRos::setTrajectoryLimits, this);
    // auto lm = LinearMotion(current_state.pose);
    // trajectory.build(current_state,lm);

    is_initialized = true;
    return true;
}

bool TrajGenRos::isInitialized()
{
    return is_initialized;
}

bool TrajGenRos::buildTrajectory(WaypointMotion waypoint_motion)
{
    ROS_DEBUG_STREAM("Building trajectory");
    trajectory_is_build = trajectory.build(current_state,waypoint_motion);
    trajectory.printInput();
    return trajectory_is_build;
}

void TrajGenRos::updateTrajectory()
{   
    if (trajectory_is_build)
    {
        // trajectory.setMaxTranslationVelocity(0.5);
        ROS_DEBUG_STREAM_ONCE("Updating trajectory");
        Result result = trajectory.update();
        if(result == (Result::Working) )
        {
            desired_state = trajectory.getNextState();
            current_state = desired_state;
        }
        else if(result == (Result::Finished) )
        {
            desired_state = trajectory.getNextState();
            current_state = desired_state;
            trajectory_is_build = false;
        }
    }
    publishTrajectory();
}

Result TrajGenRos::getTrajectoryResult()
{
    return trajectory.getTrajectoryResult();
}

Eigen::Affine3d TrajGenRos::getCartesianPose()
{
    return desired_state.pose.getFrame();
}

Eigen::Matrix<double,6,1> TrajGenRos::getCartesianVelocity()
{
    return desired_state.velocity;
}

Eigen::Matrix<double,6,1> TrajGenRos::getCartesianAcceleration()
{
    return desired_state.acceleration;
}

void TrajGenRos::publishTrajectory()
{
  tf::poseEigenToMsg(desired_state.pose.getFrame(),cartesian_pose_msg);

  tf::twistEigenToMsg(desired_state.velocity, cartesian_velocity_msg);
  tf::twistEigenToMsg(desired_state.acceleration, cartesian_acceleration_msg);
  //Publish robot desired pose
  if (pose_des_publisher.trylock())
  {
      geometry_msgs::PoseStamped X_des_stamp;
      pose_des_publisher.msg_.header.stamp = ros::Time::now();
      pose_des_publisher.msg_.header.frame_id = "world";
      pose_des_publisher.msg_.pose = cartesian_pose_msg;
      pose_des_publisher.unlockAndPublish();
  }
  //Publish robot desired velocity
  if (vel_des_publisher.trylock())
  {
      geometry_msgs::PoseStamped vel_des_stamp;
      vel_des_publisher.msg_.header.stamp = ros::Time::now();
      vel_des_publisher.msg_.header.frame_id = "world";
      vel_des_publisher.msg_.twist = cartesian_velocity_msg;
      vel_des_publisher.unlockAndPublish();
  }
  //Publish robot desired acceleration

  if (acc_des_publisher.trylock())
  {
      geometry_msgs::PoseStamped acc_des_stamp;
      acc_des_publisher.msg_.header.stamp = ros::Time::now();
      acc_des_publisher.msg_.header.frame_id = "world";
      acc_des_publisher.msg_.twist = cartesian_acceleration_msg;
      acc_des_publisher.unlockAndPublish();
  }
}

bool TrajGenRos::setTrajectoryLimits(traj_gen_ros::TrajectoryLimits::Request  &req,
         traj_gen_ros::TrajectoryLimits::Response &res)
{
    if (req.jerk_limits[0] != 0.0)
        trajectory.setMaxTranslationJerk(req.jerk_limits[0]);
    if (req.jerk_limits[1] != 0.0)
        trajectory.setMaxRotationJerk(req.jerk_limits[1]);
    if (req.acceleration_limits[0] != 0.0)
        trajectory.setMaxTranslationAcceleration(req.acceleration_limits[0]);
    if (req.acceleration_limits[1] != 0.0)
        trajectory.setMaxRotationAcceleration(req.acceleration_limits[1]);
    if (req.velocity_limits[0] != 0.0)
        trajectory.setMaxTranslationVelocity(req.velocity_limits[0]);
    if (req.velocity_limits[1] != 0.0)
        trajectory.setMaxRotationVelocity(req.velocity_limits[1]);
    trajectory.printInput();

    return true;
}

}