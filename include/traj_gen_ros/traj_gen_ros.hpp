#pragma once
#include <traj_gen/traj_gen.hpp>
#include "ros/ros.h"
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/TwistStamped.h"
#include "eigen_conversions/eigen_msg.h"
#include <dynamic_reconfigure/server.h>
#include <realtime_tools/realtime_publisher.h>
#include <geometry_msgs/TwistStamped.h>
#include <geometry_msgs/PoseStamped.h>
#include <traj_gen_ros/TrajectoryLimits.h>
namespace traj_gen_ros {

class TrajGenRos 
{
    public:
        bool initialize(Eigen::Matrix4d pose_init);
        bool buildTrajectory(WaypointMotion waypoint_motion);
        void updateTrajectory();
        Eigen::Affine3d getCartesianPose();
        Eigen::Matrix<double,6,1> getCartesianVelocity();
        Eigen::Matrix<double,6,1> getCartesianAcceleration();
        Result getTrajectoryResult();
        TrajectoryGenerator trajectory;
        bool isInitialized();
        void publishTrajectory();
        bool setTrajectoryLimits(traj_gen_ros::TrajectoryLimits::Request  &req,
                                 traj_gen_ros::TrajectoryLimits::Response &res);
    private:
        CartesianState current_state, desired_state;
        bool is_initialized = false;
        realtime_tools::RealtimePublisher<geometry_msgs::PoseStamped> pose_des_publisher;
        realtime_tools::RealtimePublisher<geometry_msgs::TwistStamped> vel_des_publisher,acc_des_publisher;
        geometry_msgs::Twist cartesian_velocity_msg,cartesian_acceleration_msg;
        ros::ServiceServer TrajectoryLimitsService;
        geometry_msgs::Pose cartesian_pose_msg;
        bool trajectory_is_build = false;
};

}